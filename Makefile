flags = -Wall -Wno-unused-function
libs = -lm
packages = sdl2 glew

ifeq ($(OS), Windows_NT)
	libs += -lopengl32 -mwindows
else
	packages += gl
endif

libs += $(shell pkg-config --cflags --libs $(packages))

polygon: src/polygon.cpp src/*
	g++ $(flags) -o $@ $< $(libs)
