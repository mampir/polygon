struct Texture {
  int width;
  int height;
  GLuint gl_id;
};


static Texture
make_dashed_texture (void)
{
  Texture tex;
  tex.width = 2;
  tex.height = 1;
  u8 image[2] = {0xff, 0x00};

  glPixelStorei (GL_UNPACK_ALIGNMENT, 1);
  glPixelTransferf (GL_RED_BIAS,   1.0f);
  glPixelTransferf (GL_GREEN_BIAS, 1.0f);
  glPixelTransferf (GL_BLUE_BIAS,  1.0f);

  glGenTextures (1, &tex.gl_id);
  glBindTexture (GL_TEXTURE_1D, tex.gl_id);
  glTexParameteri (GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri (GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage1D (GL_TEXTURE_1D, 0, GL_RGBA8,
                tex.width, 0,
                GL_ALPHA, GL_UNSIGNED_BYTE,
                image);

  glPixelStorei (GL_UNPACK_ALIGNMENT, 4);
  glPixelTransferf (GL_RED_BIAS,   0.0f);
  glPixelTransferf (GL_GREEN_BIAS, 0.0f);
  glPixelTransferf (GL_BLUE_BIAS,  0.0f);

  return tex;
}
