static void
draw_line (V3 p0, V3 p1)
{
  glBegin (GL_LINES);
  glVertex3fv (p0.e);
  glVertex3fv (p1.e);
  glEnd ();
}


static void
draw_dashed_line (V3 p0, V3 p1, u32 stride=16)
{
  V3 p2 = p0 - p1;
  r32 distance = sqrtf (p2.x * p2.x + p2.y * p2.y + p2.z * p2.z);

  glBindTexture (GL_TEXTURE_1D, _state.dashed_texture.gl_id);
  glEnable (GL_TEXTURE_1D);
  glBegin (GL_LINES);
  glTexCoord1f (0);
  glVertex3fv (p0.e);
  glTexCoord1f (distance / stride);
  glVertex3fv (p1.e);
  glEnd ();
  glDisable (GL_TEXTURE_1D);
}


static void
draw_crect (V3 pos, V2 dim)
{
  dim /= 2;
  V3 p0 = pos - (V3){dim.x, dim.y, 0};
  V3 p1 = pos + (V3){dim.x, dim.y, 0};
  glBegin (GL_TRIANGLE_STRIP);
  glVertex3f (p0.x, p0.y, p0.z);
  glVertex3f (p0.x, p1.y, p0.z);
  glVertex3f (p1.x, p0.y, p0.z);
  glVertex3f (p1.x, p1.y, p0.z);
  glEnd ();
}


static void
draw_crect (V2 pos, V2 dim)
{
  draw_crect ({pos.x, pos.y, 0}, dim);
}


static void
draw_line_crect (V2 pos, V2 dim)
{
  dim /= 2;
  V2 p0 = pos - dim;
  V2 p1 = pos + dim;
  glBegin (GL_LINE_LOOP);
  glVertex2f (p0.x, p0.y);
  glVertex2f (p0.x, p1.y);
  glVertex2f (p1.x, p1.y);
  glVertex2f (p1.x, p0.y);
  glEnd ();
}


static void
draw_xz_crect (V3 pos, V2 dim)
{
  dim /= 2;
  V2 p0;
  p0.x = pos.x - dim.x;
  p0.y = pos.y - dim.y;
  V2 p1;
  p1.x = pos.x + dim.x;
  p1.y = pos.y + dim.y;

  glBegin (GL_TRIANGLE_STRIP);
  glVertex3f (p0.x, p0.y, pos.z);
  glVertex3f (p0.x, p1.y, pos.z);
  glVertex3f (p1.x, p0.y, pos.z);
  glVertex3f (p1.x, p1.y, pos.z);
  glEnd ();
}


static void
draw_point_cbox (V3 pos, V3 dim)
{
  dim /= 2;
  r32 lef = pos.x - dim.x;
  r32 rig = pos.x + dim.x;
  r32 bot = pos.y - dim.y;
  r32 top = pos.y + dim.y;
  r32 bac = pos.z - dim.z;
  r32 fro = pos.z + dim.z;

  glBegin (GL_POINTS);
  glVertex3f (lef, bot, bac);
  glVertex3f (rig, bot, bac);
  glVertex3f (lef, bot, fro);
  glVertex3f (rig, bot, fro);

  glVertex3f (lef, top, bac);
  glVertex3f (rig, top, bac);
  glVertex3f (lef, top, fro);
  glVertex3f (rig, top, fro);
  glEnd ();
}


static void
draw_line_cbox (V3 pos, V3 dim)
{
  dim /= 2;
  r32 lef = pos.x - dim.x;
  r32 rig = pos.x + dim.x;
  r32 bot = pos.y - dim.y;
  r32 top = pos.y + dim.y;
  r32 bac = pos.z - dim.z;
  r32 fro = pos.z + dim.z;

  glBegin (GL_LINES);
  glVertex3f (lef, bot, bac);
  glVertex3f (rig, bot, bac);

  glVertex3f (lef, bot, fro);
  glVertex3f (rig, bot, fro);

  glVertex3f (lef, bot, bac);
  glVertex3f (lef, bot, fro);

  glVertex3f (rig, bot, bac);
  glVertex3f (rig, bot, fro);

  glVertex3f (lef, top, bac);
  glVertex3f (rig, top, bac);

  glVertex3f (lef, top, fro);
  glVertex3f (rig, top, fro);

  glVertex3f (lef, top, bac);
  glVertex3f (lef, top, fro);

  glVertex3f (rig, top, bac);
  glVertex3f (rig, top, fro);

  glVertex3f (lef, bot, bac);
  glVertex3f (lef, top, bac);

  glVertex3f (rig, bot, bac);
  glVertex3f (rig, top, bac);

  glVertex3f (lef, bot, fro);
  glVertex3f (lef, top, fro);

  glVertex3f (rig, bot, fro);
  glVertex3f (rig, top, fro);
  glEnd ();
}


static void
draw_axis (V3 pos)
{
  r32 size = 100;

  glBegin (GL_LINES);
  glColor3f (1, 0, 0);
  glVertex3f (pos.x, pos.y, pos.z);
  glVertex3f (pos.x + size, pos.y, pos.z);

  glColor3f (0, 1, 0);
  glVertex3f (pos.x, pos.y, pos.z);
  glVertex3f (pos.x, pos.y + size, pos.z);

  glColor3f (0, 0, 1);
  glVertex3f (pos.x, pos.y, pos.z);
  glVertex3f (pos.x, pos.y, pos.z + size);
  glEnd ();

  glColor3f (1, 1, 1);
}


static V2
draw_matrix (V2 pos, Mat4x4 m)
{
  return draw_text (pos,
                    "%5.2f%5.2f%5.2f%5.2f\n"
                    "%5.2f%5.2f%5.2f%5.2f\n"
                    "%5.2f%5.2f%5.2f%5.2f\n"
                    "%5.2f%5.2f%5.2f%5.2f",
                    m.m[ 0], m.m[ 1], m.m[ 2], m.m[ 3],
                    m.m[ 4], m.m[ 5], m.m[ 6], m.m[ 7],
                    m.m[ 8], m.m[ 9], m.m[10], m.m[11],
                    m.m[12], m.m[13], m.m[14], m.m[15]);
}


static void
draw_image (V2 pos, V2 dim, GLuint tex)
{
  V2 p0 = pos;
  V2 p1 = pos + dim;

  glBindTexture (GL_TEXTURE_2D, tex);
  glEnable (GL_TEXTURE_2D);

  glBegin (GL_TRIANGLE_STRIP);
  glTexCoord2f (0, 0);
  glVertex2f (p0.x, p0.y);
  glTexCoord2f (0, 1);
  glVertex2f (p0.x, p1.y);
  glTexCoord2f (1, 0);
  glVertex2f (p1.x, p0.y);
  glTexCoord2f (1, 1);
  glVertex2f (p1.x, p1.y);
  glEnd ();

  glDisable (GL_TEXTURE_2D);
}


static void
draw_rect (V2 pos, V2 dim)
{
  V2 p0 = pos;
  V2 p1 = pos + dim;

  glBegin (GL_TRIANGLE_STRIP);
  glVertex2f (p0.x, p0.y);
  glVertex2f (p0.x, p1.y);
  glVertex2f (p1.x, p0.y);
  glVertex2f (p1.x, p1.y);
  glEnd ();

  glDisable (GL_TEXTURE_2D);
}


static void
draw_line_circle (V2 pos, r32 r)
{
  u32 steps = 12;
  r32 step = M_PI * 2 / steps;
  r32 a = step;

  glBegin (GL_LINE_LOOP);
  glVertex2f (pos.x + r, pos.y);
  for (u8 i = 1; i < steps; ++i)
    {
      glVertex2f (cosf (a) * r + pos.x, sinf (a) * r + pos.y);
      a += step;
    }
  glEnd ();
}


static void
draw_clock (V2 pos, r32 r, r32 a)
{
  V2 p1 = pos;
  p1.x += cosf (a) * r;
  p1.y += sinf (a) * r;

  draw_line_circle (pos, r + 4);
  glBegin (GL_LINES);
  glVertex2fv (pos.e);
  glVertex2fv (p1.e);
  glEnd ();
}
