struct Font {
  u32 glyph_w;
  u32 glyph_h;
  u32 glyph_count;
  Texture foreground;
  Texture background;
  int watch_id;
  b8 changed;
  const char *filepath;
};


static void
load_font_data (Font *font)
{
  printf ("load font data: %s\n", font->filepath);
  size_t foreground_datasize = font->foreground.width * font->foreground.height;
  size_t background_datasize = font->background.width * font->background.height;

  FILE *foreground_file = fopen (font->filepath, "rb");
  assert (foreground_file);

  u8 *foreground_data = MALLOC (u8, foreground_datasize);
  size_t bytes_read = fread (foreground_data, 1, foreground_datasize, foreground_file);
  assert (bytes_read == foreground_datasize);

  fclose (foreground_file);

  u8 *background_data = (u8 *) calloc (1, background_datasize);

  for (int y = 0; y < font->foreground.height; ++y)
    {
      u8 *row = foreground_data + (y * font->foreground.width);
      for (int x = 0; x < font->foreground.width; ++x)
        {
          if (row[x])
            {
              int glyph = x / font->glyph_w;
              int bg_x = x + glyph * 2;
              background_data[(y+0) * font->background.width + bg_x+0] = 0xff;
              background_data[(y+0) * font->background.width + bg_x+1] = 0xff;
              background_data[(y+0) * font->background.width + bg_x+2] = 0xff;
              background_data[(y+1) * font->background.width + bg_x+0] = 0xff;
              background_data[(y+1) * font->background.width + bg_x+1] = 0xff;
              background_data[(y+1) * font->background.width + bg_x+2] = 0xff;
              background_data[(y+2) * font->background.width + bg_x+0] = 0xff;
              background_data[(y+2) * font->background.width + bg_x+1] = 0xff;
              background_data[(y+2) * font->background.width + bg_x+2] = 0xff;
            }
        }
    }

  glPixelStorei (GL_UNPACK_ALIGNMENT, 1);
  glPixelTransferf (GL_RED_BIAS,   1.0f);
  glPixelTransferf (GL_GREEN_BIAS, 1.0f);
  glPixelTransferf (GL_BLUE_BIAS,  1.0f);

  glGenTextures (1, &font->foreground.gl_id);
  glBindTexture (GL_TEXTURE_2D, font->foreground.gl_id);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA8,
                font->foreground.width, font->foreground.height, 0,
                GL_ALPHA, GL_UNSIGNED_BYTE,
                foreground_data);

  glGenTextures (1, &font->background.gl_id);
  glBindTexture (GL_TEXTURE_2D, font->background.gl_id);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA8,
                font->background.width, font->background.height, 0,
                GL_ALPHA, GL_UNSIGNED_BYTE,
                background_data);

  glPixelStorei (GL_UNPACK_ALIGNMENT, 4);
  glPixelTransferf (GL_RED_BIAS,   0.0f);
  glPixelTransferf (GL_GREEN_BIAS, 0.0f);
  glPixelTransferf (GL_BLUE_BIAS,  0.0f);

  // printf ("load_font_data (%s):\n", font->filepath);
  // debug_print (font->foreground.gl_id);
  // debug_print (font->background.gl_id);

  free (foreground_data);
  free (background_data);
}


static Font
load_font (const char *filepath, u32 glyph_w, u32 glyph_h, u32 glyph_count)
{
  Font font = {};
  font.glyph_w = glyph_w;
  font.glyph_h = glyph_h;
  font.glyph_count = glyph_count;
  font.filepath = filepath;
  // font.watch_id = inotify_add_watch (state.inotify_fd, font.filepath, IN_CLOSE_WRITE);
  // debug_print (font.watch_id);

  font.foreground.width  = font.glyph_w * font.glyph_count;
  font.foreground.height = font.glyph_h;
  font.background.width  = (font.glyph_w + 2) * font.glyph_count;
  font.background.height = (font.glyph_h + 2);

  load_font_data (&font);

  return font;
}
