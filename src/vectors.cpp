union V3 {
  struct {r32 x, y, z;};
  r32 e[3];
};

union V2 {
  struct {r32 x, y;};
  struct {r32 w, h;};
  r32 e[2];
};

union Quaternion {
  struct {r32 x, y, z, w;};
  r32 e[4];
};

struct Mat4x4 {
  r32 m[16];
};

static b8
operator== (V2 a, V2 b)
{
  return a.x == b.x && a.y == b.y;
}

static b8
operator!= (V2 a, V2 b)
{
  return a.x != b.x && a.y != b.y;
}

static V2
operator- (V2 a, V2 b)
{
  return {a.x - b.x, a.y - b.y};
}

static V2
operator+ (V2 a, V2 b)
{
  return {a.x + b.x, a.y + b.y};
}

static V2
operator* (V2 a, V2 b)
{
  return {a.x * b.x, a.y * b.y};
}

static V2
operator- (V2 v, r32 n)
{
  return {v.x - n, v.y - n};
}

static V2
operator+ (V2 v, r32 n)
{
  return {v.x + n, v.y + n};
}

static V2
operator* (V2 v, r32 n)
{
  return {v.x * n, v.y * n};
}

static V2
operator/ (V2 v, r32 n)
{
  return {v.x / n, v.y / n};
}

static V2
operator/= (V2 &v, r32 n)
{
  v.x /= n;
  v.y /= n;
  return v;
}

static V3
operator- (V3 a, V3 b)
{
  return {a.x - b.x, a.y - b.y, a.z - b.z};
}

static V3
operator+ (V3 a, V3 b)
{
  return {a.x + b.x, a.y + b.y, a.z + b.z};
}

static V3
operator/ (V3 v, r32 n)
{
  return {v.x / n, v.y / n, v.z / n};
}

static V3
operator/= (V3 &v, r32 n)
{
  v.x /= n;
  v.y /= n;
  v.z /= n;
  return v;
}

static V3
operator+= (V3 &v, V2 v2)
{
  v.x += v2.x;
  v.y += v2.y;
  return v;
}

static V3
operator+= (V3 &v, V3 v2)
{
  v.x += v2.x;
  v.y += v2.y;
  v.z += v2.z;
  return v;
}

static V3
operator-= (V3 &v, V2 v2)
{
  v.x -= v2.x;
  v.y -= v2.y;
  return v;
}

static V3
operator-= (V3 &v, V3 v2)
{
  v.x -= v2.x;
  v.y -= v2.y;
  v.z -= v2.z;
  return v;
}

static Mat4x4
operator* (Mat4x4 a, Mat4x4 b)
{
  Mat4x4 r;
  r.m[0]  = a.m[ 0]*b.m[0] + a.m[ 1]*b.m[4] + a.m[ 2]*b.m[ 8] + a.m[ 3]*b.m[12];
  r.m[1]  = a.m[ 0]*b.m[1] + a.m[ 1]*b.m[5] + a.m[ 2]*b.m[ 9] + a.m[ 3]*b.m[13];
  r.m[2]  = a.m[ 0]*b.m[2] + a.m[ 1]*b.m[6] + a.m[ 2]*b.m[10] + a.m[ 3]*b.m[14];
  r.m[3]  = a.m[ 0]*b.m[3] + a.m[ 1]*b.m[7] + a.m[ 2]*b.m[11] + a.m[ 3]*b.m[15];

  r.m[4]  = a.m[ 4]*b.m[0] + a.m[ 5]*b.m[4] + a.m[ 6]*b.m[ 8] + a.m[ 7]*b.m[12];
  r.m[5]  = a.m[ 4]*b.m[1] + a.m[ 5]*b.m[5] + a.m[ 6]*b.m[ 9] + a.m[ 7]*b.m[13];
  r.m[6]  = a.m[ 4]*b.m[2] + a.m[ 5]*b.m[6] + a.m[ 6]*b.m[10] + a.m[ 7]*b.m[14];
  r.m[7]  = a.m[ 4]*b.m[3] + a.m[ 5]*b.m[7] + a.m[ 6]*b.m[11] + a.m[ 7]*b.m[15];

  r.m[8]  = a.m[ 8]*b.m[0] + a.m[ 9]*b.m[4] + a.m[10]*b.m[ 8] + a.m[11]*b.m[12];
  r.m[9]  = a.m[ 8]*b.m[1] + a.m[ 9]*b.m[5] + a.m[10]*b.m[ 9] + a.m[11]*b.m[13];
  r.m[10] = a.m[ 8]*b.m[2] + a.m[ 9]*b.m[6] + a.m[10]*b.m[10] + a.m[11]*b.m[14];
  r.m[11] = a.m[ 8]*b.m[3] + a.m[ 9]*b.m[7] + a.m[10]*b.m[11] + a.m[11]*b.m[15];

  r.m[12] = a.m[12]*b.m[0] + a.m[13]*b.m[4] + a.m[14]*b.m[ 8] + a.m[15]*b.m[12];
  r.m[13] = a.m[12]*b.m[1] + a.m[13]*b.m[5] + a.m[14]*b.m[ 9] + a.m[15]*b.m[13];
  r.m[14] = a.m[12]*b.m[2] + a.m[13]*b.m[6] + a.m[14]*b.m[10] + a.m[15]*b.m[14];
  r.m[15] = a.m[12]*b.m[3] + a.m[13]*b.m[7] + a.m[14]*b.m[11] + a.m[15]*b.m[15];

  return r;
}

static Mat4x4
get_x_rotate_transform (r32 a)
{
  r32 c = cosf (a);
  r32 s = sinf (a);
  Mat4x4 m = {
    1,  0,  0,  0,
    0,  c, -s,  0,
    0,  s,  c,  0,
    0,  0,  0,  1,
  };
  return m;
}


static Mat4x4
get_y_rotate_transform (r32 a)
{
  r32 c = cosf (a);
  r32 s = sinf (a);
  Mat4x4 m = {
    +c,  0,  s,  0,
    +0,  1,  0,  0,
    -s,  0,  c,  0,
    +0,  0,  0,  1,
  };
  return m;
}


static Mat4x4
get_z_rotate_transform (r32 a)
{
  r32 c = cosf (a);
  r32 s = sinf (a);
  Mat4x4 m = {
    c, -s,  0,  0,
    s,  c,  0,  0,
    0,  0,  1,  0,
    0,  0,  0,  1,
  };
  return m;
}


static V2
operator* (V2 v, Mat4x4 m)
{
  V2 r;
  r.x = v.x * m.m[0] + v.y * m.m[1] + m.m[ 3];
  r.y = v.x * m.m[4] + v.y * m.m[5] + m.m[ 7];
  return r;
}

static V3
operator* (V3 v, Mat4x4 m)
{
  V3 r;
  r.x = v.x * m.m[0] + v.y * m.m[1] + v.z * m.m[ 2] + m.m[ 3];
  r.y = v.x * m.m[4] + v.y * m.m[5] + v.z * m.m[ 6] + m.m[ 7];
  r.z = v.x * m.m[8] + v.y * m.m[9] + v.z * m.m[10] + m.m[11];
  return r;
}

static V2
v2 (V3 v)
{
  return {v.x, v.y};
}

static V3
v3 (V2 v)
{
  return {v.x, v.y, 0};
}

static r32
get_distance (V2 a, V2 b)
{
  V2 c = a - b;
  r32 distance = sqrtf (c.x*c.x + c.y*c.y);
  return distance;
}


static Quaternion
operator* (Quaternion a, Quaternion b)
{
  Quaternion r;
  r.x =  a.x * b.w + a.y * b.z - a.z * b.y + a.w * b.x;
  r.y = -a.x * b.z + a.y * b.w + a.z * b.x + a.w * b.y;
  r.z =  a.x * b.y - a.y * b.x + a.z * b.w + a.w * b.z;
  r.w = -a.x * b.x - a.y * b.y - a.z * b.z + a.w * b.w;
  return r;
}


static Quaternion
operator*= (Quaternion &a, Quaternion b)
{
  a = a * b;
  return a;
}


static Quaternion
get_inverse_quaternion (Quaternion q)
{
  q.x = -q.x;
  q.y = -q.y;
  q.z = -q.z;
  return q;
}

static Mat4x4
quaternion_to_mat4x4 (Quaternion q)
{
  float x2 = q.x * q.x;
  float y2 = q.y * q.y;
  float z2 = q.z * q.z;
  float xy = q.x * q.y;
  float xz = q.x * q.z;
  float yz = q.y * q.z;
  float wx = q.w * q.x;
  float wy = q.w * q.y;
  float wz = q.w * q.z;
  Mat4x4 m = {
              1-2*(y2 + z2),   2*(xy - wz),   2*(xz + wy), 0,
                2*(xy + wz), 1-2*(x2 + z2),   2*(yz - wx), 0,
                2*(xz - wy),   2*(yz + wx), 1-2*(x2 + y2), 0,
                          0,             0,             0, 1,
  };
  return m;
}
