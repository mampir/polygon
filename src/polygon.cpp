#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL.h>

#include <math.h>
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>


#define ARRAY_LEN(arr) (sizeof (arr) / sizeof ((arr)[0]))
#define MALLOC(type, count) (type *) malloc (sizeof (type) * (count))
#define REALLOC(ptr, type, count) (type *) realloc (ptr, sizeof (type) * (count))
#define STRING(cstr) ((String) {(char *) cstr, sizeof (cstr) - 1})


typedef uint8_t  b8;
typedef uint8_t  u8;
typedef  int8_t  s8;
typedef uint32_t u32;
typedef float    r32;
typedef double   r64;


#include "vectors.cpp"

struct Vertex {
  V3 pos;
  b8 selected;
};

#include "textures.cpp"
#include "font.cpp"

struct State {
  Font font;
  Texture dashed_texture;
  r32  text_scale = 1;
} _state;

struct Input {
  s8 x_rotate_toggle;
  s8 y_rotate_toggle;
  s8 z_rotate_toggle;
  b8 points_toggle = 1;
  b8 lines_toggle  = 1;
  b8 coords_toggle = 1;
  b8 play_toggle = 1;
  b8 grab;
  b8 select;
  b8 cancel;
  b8 remove;

  b8 rotate_xmore;
  b8 rotate_xless;
  b8 rotate_ymore;
  b8 rotate_yless;
  b8 rotate_zmore;
  b8 rotate_zless;

  r32 ltrigger;
  r32 rtrigger;
  r32 leftx;
  r32 lefty;
  r32 rightx;
  r32 righty;

  b8 reset_rotation;
  b8 insert_point;
  b8 take_screenshot;
} _input;


#include "draw_text.cpp"
#include "draw.cpp"


static V2
get_segment_intersection (V2 p0, V2 p1, V2 p2, V2 p3)
{
  r32 A1 = p1.y - p0.y;
  r32 B1 = p0.x - p1.x;
  r32 C1 = A1 * p0.x + B1 * p0.y;
  r32 A2 = p3.y - p2.y;
  r32 B2 = p2.x - p3.x;
  r32 C2 = A2 * p2.x + B2 * p2.y;
  r32 denominator = A1 * B2 - A2 * B1;
  if (!denominator) return {0, 0};

  V2 r;
  r.x = (B2 * C1 - B1 * C2) / denominator;
  r.y = (A1 * C2 - A2 * C1) / denominator;

  r32 rx0 = (r.x - p0.x) / (p1.x - p0.x);
  r32 ry0 = (r.y - p0.y) / (p1.y - p0.y);
  r32 rx1 = (r.x - p2.x) / (p3.x - p2.x);
  r32 ry1 = (r.y - p2.y) / (p3.y - p2.y);
  if (((rx0 >= 0 && rx0 <= 1) || (ry0 >= 0 && ry0 <= 1)) &&
      ((rx1 >= 0 && rx1 <= 1) || (ry1 >= 0 && ry1 <= 1)))
    {
      return r;
    }
  else
    {
      return {NAN, NAN};
    }
}


int
main (int argc, char *argv[])
{
  V2 window_dim = {500, 500};

  SDL_Init (SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER);
  SDL_GameControllerEventState (SDL_ENABLE);
  SDL_GameControllerOpen (0);

  SDL_Window *window = SDL_CreateWindow ("Polygon", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, window_dim.w, window_dim.h, SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);
  assert (window);
  SDL_GLContext gl_context = SDL_GL_CreateContext (window);
  assert (gl_context);
  glewExperimental = GL_TRUE;
  glewInit ();

  glEnable (GL_BLEND);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable (GL_DEPTH_TEST);

  _state.font = load_font ("res/font6x10.data", 6, 10, 95);
  _state.dashed_texture = make_dashed_texture ();

  V2 mouse_pos = {};
  u32 points_max = 4096;
  u32 points_count = 2;
  Vertex points[points_max];
  points[0] = {15, -15, 0, 0};
  points[1] = {10, -20, -40, 0};
  b8 grab_mode = 0;
  V2 grab_pos;
  V2 grab_delta;

  V2     screenshot_dim = {};
  GLuint screenshot_tex;
  glGenTextures (1, &screenshot_tex);
  glBindTexture (GL_TEXTURE_2D, screenshot_tex);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  Uint32 last_time = SDL_GetTicks ();
  for (int keep_running = 1; keep_running;)
    {
      Uint32 current_time = SDL_GetTicks ();
      Uint32 frame_time = current_time - last_time;
      r64 dt = frame_time / 1000.0;
      last_time = current_time;
      for (SDL_Event event; SDL_PollEvent (&event);)
        {
          if (event.type == SDL_QUIT)
            {
              keep_running = 0;
            }
          else if (event.type == SDL_MOUSEMOTION)
            {
              mouse_pos.x = event.motion.x - window_dim.w / 2;
              mouse_pos.y = window_dim.h - event.motion.y - window_dim.h / 2;
            }
          else if (event.type == SDL_KEYUP)
            {
              switch (event.key.keysym.sym)
                {
                case SDLK_s:   _input.rotate_xless = 0; break;
                case SDLK_w:   _input.rotate_xmore = 0; break;
                case SDLK_a:   _input.rotate_yless = 0; break;
                case SDLK_d:   _input.rotate_ymore = 0; break;
                case SDLK_q:   _input.rotate_zless = 0; break;
                case SDLK_e:   _input.rotate_zmore = 0; break;
                }
            }
          else if (event.type == SDL_KEYDOWN)
            {
              switch (event.key.keysym.sym)
                {
                case SDLK_x:   _input.remove = 1; break;
                case SDLK_g:   _input.grab = 1; break;
                case SDLK_s:   _input.rotate_xless = 1; break;
                case SDLK_w:   _input.rotate_xmore = 1; break;
                case SDLK_a:   _input.rotate_yless = 1; break;
                case SDLK_d:   _input.rotate_ymore = 1; break;
                case SDLK_q:   _input.rotate_zless = 1; break;
                case SDLK_e:   _input.rotate_zmore = 1; break;

                case SDLK_F1:  _input.x_rotate_toggle = (_input.x_rotate_toggle + 3) % 5 - 2; break;
                case SDLK_F2:  _input.y_rotate_toggle = (_input.y_rotate_toggle + 3) % 5 - 2; break;
                case SDLK_F3:  _input.z_rotate_toggle = (_input.z_rotate_toggle + 3) % 5 - 2; break;
                case SDLK_F5:  _input.points_toggle = !_input.points_toggle; break;
                case SDLK_F6:  _input. lines_toggle = !_input. lines_toggle; break;
                case SDLK_F7:  _input.coords_toggle = !_input.coords_toggle; break;
                case SDLK_p:   _input.play_toggle = !_input.play_toggle; break;

                case SDLK_r:   _input.reset_rotation  = 1; break;
                case SDLK_i:   _input.insert_point    = 1; break;
                case SDLK_F12: _input.take_screenshot = 1; break;
                }
            }
          else if (event.type == SDL_MOUSEBUTTONDOWN)
            {
              if (event.button.button == SDL_BUTTON_LEFT)
                {
                  _input.select = 1;
                }
               if (event.button.button == SDL_BUTTON_RIGHT)
                {
                  _input.cancel = 1;
                }
            }
          else if (event.type == SDL_CONTROLLERAXISMOTION)
            {
              switch (event.caxis.axis)
                {
                case SDL_CONTROLLER_AXIS_TRIGGERLEFT: _input.ltrigger = event.caxis.value / 32768.0; break;
                case SDL_CONTROLLER_AXIS_TRIGGERRIGHT: _input.rtrigger = event.caxis.value / 32768.0; break;
                case SDL_CONTROLLER_AXIS_LEFTX: _input.leftx = event.caxis.value / 32768.0; break;
                case SDL_CONTROLLER_AXIS_LEFTY: _input.lefty = event.caxis.value / 32768.0; break;
                case SDL_CONTROLLER_AXIS_RIGHTX: _input.righty = event.caxis.value / 32768.0; break;
                case SDL_CONTROLLER_AXIS_RIGHTY: _input.rightx = event.caxis.value / 32768.0; break;
                }
            }
          else if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
            {
              window_dim.w = event.window.data1;
              window_dim.h = event.window.data2;
              glViewport (0, 0, window_dim.w, window_dim.h);
            }
        }

      glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      r32 w = 2 / window_dim.w;
      r32 h = 2 / window_dim.h;
      r32 z = 2 / 1280.0;
      Mat4x4 aspect_transform = {
        w, 0, 0, 0,
        0, h, 0, 0,
        0, 0, z, 0,
        0, 0, 0, 1,
      };

      static Quaternion orientation = {0, 0, 0, 1};
      if (_input.reset_rotation) {_input.reset_rotation = 0; orientation = {0, 0, 0, 1};}

      r32 rotation_d = M_PI / 2 * dt / 2;

      orientation = (Quaternion){sinf (_input.x_rotate_toggle / 4.0f * rotation_d), 0, 0, cosf (_input.x_rotate_toggle / 4.0f * rotation_d)} * orientation;
      orientation = (Quaternion){0, sinf (_input.y_rotate_toggle / 4.0f * rotation_d), 0, cosf (_input.y_rotate_toggle / 4.0f * rotation_d)} * orientation;
      orientation = (Quaternion){0, 0, sinf (_input.z_rotate_toggle / 4.0f * rotation_d), cosf (_input.z_rotate_toggle / 4.0f * rotation_d)} * orientation;

      r32 trigger = _input.ltrigger - _input.rtrigger;
      orientation = (Quaternion){sinf (_input.rightx * rotation_d), 0, 0, cosf (_input.rightx * rotation_d)} * orientation;
      orientation = (Quaternion){0, sinf (_input.righty * rotation_d), 0, cosf (_input.righty * rotation_d)} * orientation;
      orientation = (Quaternion){0, 0, sinf (trigger       * rotation_d), cosf (trigger       * rotation_d)} * orientation;

      if (_input.rotate_xless) orientation = (Quaternion){sinf (-rotation_d), 0, 0, cosf (-rotation_d)} * orientation;
      if (_input.rotate_xmore) orientation = (Quaternion){sinf ( rotation_d), 0, 0, cosf ( rotation_d)} * orientation;
      if (_input.rotate_yless) orientation = (Quaternion){0, sinf (-rotation_d), 0, cosf (-rotation_d)} * orientation;
      if (_input.rotate_ymore) orientation = (Quaternion){0, sinf ( rotation_d), 0, cosf ( rotation_d)} * orientation;
      if (_input.rotate_zless) orientation = (Quaternion){0, 0, sinf (-rotation_d), cosf (-rotation_d)} * orientation;
      if (_input.rotate_zmore) orientation = (Quaternion){0, 0, sinf ( rotation_d), cosf ( rotation_d)} * orientation;

      Mat4x4 graphics_transform     = quaternion_to_mat4x4 (orientation);
      Mat4x4 graphics_transform_inv = quaternion_to_mat4x4 (get_inverse_quaternion (orientation));
      glLoadTransposeMatrixf ((aspect_transform * graphics_transform).m);

      if (_input.select)
        {
          _input.select = 0;
          for (u32 i = 0; i < points_count; i++)
            {
              V2 point = v2 (points[i].pos * graphics_transform);
              r32 dis = get_distance (point, mouse_pos);
              if (dis <= 5)
                {
                  points[i].selected = !points[i].selected;
                  break;
                }
            }
        }

      if (_input.cancel)
        {
          _input.cancel = 0;
          grab_mode = 0;
          if (grab_delta != (V2){})
            {
              for (u32 i = 0; i < points_count; i++)
                {
                  if (points[i].selected)
                    {
                      points[i].pos -= grab_delta;
                    }
                }
            }
        }

      if (_input.remove)
        {
          _input.remove = 0;
          if (!grab_mode)
            {
              u32 shift = 0;
              for (u32 i = 0; i < points_count; i++)
                {
                  if (points[i].selected)
                    {
                      shift++;
                    }
                  else
                    {
                      points[i - shift] = points[i];
                    }
                }
              points_count -= shift;
            }
        }

      if (_input.grab)
        {
          _input.grab = 0;
          for (u32 i = 0; i < points_count; i++)
            {
              if (points[i].selected)
                {
                  grab_mode = !grab_mode;
                  grab_pos = mouse_pos;
                  grab_delta = {};
                  break;
                }
            }
        }
      if (grab_mode)
        {
          V2 d1 = mouse_pos - grab_pos;
          V2 d2 = d1 - grab_delta;
          //if (d2 != (V2){})
            {
              for (u32 i = 0; i < points_count; i++)
                {
                  if (points[i].selected)
                    {
                      points[i].pos += (V3){d2.x, d2.y, 0} * graphics_transform_inv;
                    }
                }
            }
          grab_delta = d1;
        }

      if (_input.insert_point)
        {
          _input.insert_point = 0;
          if (points_count < points_max)
            {
              Vertex point = {};
              point.pos = (V3){mouse_pos.x, mouse_pos.y, 0} * graphics_transform_inv;
              points[points_count++] = point;
            }
        }

      draw_axis ({0,0,0});

      if (_input.lines_toggle)
        {
          glBegin (GL_LINE_STRIP);
          for (u32 i = 0; i < points_count; ++i)
            {
              V3 point = points[i].pos;
              glVertex3fv (point.e);
            }
          glEnd ();
        }

      draw_line_cbox ({0,0,0}, {100,100,100});

      glColor3f (0,0,0.8);
      draw_xz_crect ({-20, 20,  20}, {80, 80});
      glColor3f (0,0.6,0);
      draw_xz_crect ({ 20,-20, -20}, {80, 80});
      glColor3f (1,1,1);

      V3 view_axis = (V3){300,0,0} * graphics_transform_inv;
      draw_dashed_line ((V3){0,0,0}, view_axis);

      glLoadTransposeMatrixf (aspect_transform.m);

      if (_input.points_toggle)
        {
          for (u32 i = 0; i < points_count; ++i)
            {
              V3 point = points[i].pos * graphics_transform;
              if (points[i].selected)
                {
                  glColor3f (1, 1, 0);
                }
              draw_crect (point, {6, 6});
              glColor3f (1, 1, 1);
            }
        }

      if (_input.coords_toggle)
        {
          for (u32 i = 0; i < points_count; ++i)
            {
              V3 point = points[i].pos * graphics_transform;
              draw_text (v2 (point), "%+06.1f,%+06.1f,%+06.1f", point.x, point.y, point.z);
            }
        }

      Mat4x4 gui_transform = aspect_transform;
      gui_transform.m[3] = -1;
      gui_transform.m[7] = -1;
      glLoadTransposeMatrixf (gui_transform.m);

      V2 tl_cursor_pos = {2, window_dim.h - (int) (_state.font.glyph_h * 1.5)};
      tl_cursor_pos.x = draw_matrix (tl_cursor_pos, graphics_transform).x;
      tl_cursor_pos.x += _state.font.glyph_w;
      draw_text (tl_cursor_pos, "%+05.2f,%+05.2f,%+05.2f,%+05.2f", orientation.x, orientation.y, orientation.z, orientation.w);
      tl_cursor_pos.y -= _state.font.glyph_h + 5;
      tl_cursor_pos.x += _state.font.glyph_w * 2 + 3;
      draw_clock (tl_cursor_pos, 10, -orientation.x * (M_PI * 2)); //  + M_PI / 2
      tl_cursor_pos.x += _state.font.glyph_w * 6;
      draw_clock (tl_cursor_pos, 10, -orientation.y * (M_PI * 2)); //  + M_PI / 2
      tl_cursor_pos.x += _state.font.glyph_w * 6;
      draw_clock (tl_cursor_pos, 10, -orientation.z * (M_PI * 2)); //  + M_PI / 2
      tl_cursor_pos.x += _state.font.glyph_w * 6;
      draw_clock (tl_cursor_pos, 10, -orientation.w * (M_PI * 2)); //  + M_PI / 2

      draw_text ({2, 0}, "w:%dx%d m:%+04d,%+04d",
                 (int) window_dim.w, (int) window_dim.h,
                 (int) mouse_pos.x, (int) mouse_pos.y);

      if (_input.take_screenshot)
        {
          _input.take_screenshot = 0;

          screenshot_dim = {400.0f, 400.0f};
          u32 screenshot_buffer_w = screenshot_dim.w;
          u32 screenshot_buffer_h = screenshot_dim.h;
          u32 screenshot_buffer_size = screenshot_buffer_w * screenshot_buffer_h * 4;
          u8  screenshot_buffer[screenshot_buffer_size];

          V2 frame_pos = window_dim / 2 - screenshot_dim / 2;

          glReadPixels (frame_pos.x, frame_pos.y, screenshot_buffer_w, screenshot_buffer_h, GL_RGBA, GL_UNSIGNED_BYTE, screenshot_buffer);
          glBindTexture (GL_TEXTURE_2D, screenshot_tex);
          glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA8,
                        screenshot_buffer_w, screenshot_buffer_h, 0,
                        GL_RGBA, GL_UNSIGNED_BYTE,
                        screenshot_buffer);
        }

      if (screenshot_dim.w)
        {
          V2 image_pos = {10,30};
          V2 image_dim = screenshot_dim / 4;
          draw_image (image_pos,     image_dim, screenshot_tex);
          draw_rect  (image_pos - 2, image_dim + 4);
        }

      SDL_GL_SwapWindow (window);
    }

  return 0;
}
