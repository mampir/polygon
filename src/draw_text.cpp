static V2
draw_string (V2 pos, const char *s)
{
  r32 tw = 1.0f / _state.font.glyph_count;
  r32 glyph_w = _state.font.glyph_w * _state.text_scale;
  r32 glyph_h = _state.font.glyph_h * _state.text_scale;

  glEnable (GL_TEXTURE_2D);
  glBindTexture (GL_TEXTURE_2D, _state.font.foreground.gl_id);

  V2 cur = pos;

  for (char c = s[0]; c; c=(++s)[0])
    {
      if (c == '\n')
        {
          cur.x = pos.x;
          cur.y -= glyph_h;
          continue;
        }

      u32 glyph_index = c - 32;
      r32 tx0 = tw * glyph_index;
      r32 tx1 = tx0 + tw;

      glBegin (GL_TRIANGLE_STRIP);
      glTexCoord2f (tx0, 1);
      glVertex2f (cur.x, cur.y);
      glTexCoord2f (tx0, 0);
      glVertex2f (cur.x, cur.y + glyph_h);

      cur.x += glyph_w;

      glTexCoord2f (tx1, 1);
      glVertex2f (cur.x, cur.y);
      glTexCoord2f (tx1, 0);
      glVertex2f (cur.x, cur.y + glyph_h);
      glEnd ();
    }

  glDisable (GL_TEXTURE_2D);

  return cur;
}

static V2
draw_text_va_list (V2 pos, const char *format, va_list format_args)
{
  char buffer[4096];
  vsnprintf (buffer, sizeof (buffer), format, format_args);
  return draw_string (pos, buffer);
}

static V2
draw_text (V2 pos, const char *format, ...)  __attribute__ ((__format__ (__printf__, 2, 3)));
static V2
draw_text (V2 pos, const char *format, ...)
{
  va_list format_args;
  va_start (format_args, format);
  pos = draw_text_va_list (pos, format, format_args);
  va_end (format_args);
  return pos;
}
